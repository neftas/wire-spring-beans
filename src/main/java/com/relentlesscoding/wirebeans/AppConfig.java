package com.relentlesscoding.wirebeans;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Configuration
@ComponentScan
public class AppConfig {
    @Bean
    public List<Streak> streaks() {
        List<Streak> streaks = new ArrayList<>();
        streaks.add(new PositiveStreak(LocalDate.now()));
        return streaks;
    }
}
