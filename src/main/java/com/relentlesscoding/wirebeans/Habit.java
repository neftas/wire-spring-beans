package com.relentlesscoding.wirebeans;

import java.util.Collection;

public interface Habit {
    String getName();
    String getDescription();
    Collection<Streak> getStreaks();
    void addStreak(Streak streak);
}
