Very simple example project that aims to explain the basics of Spring's bean wiring and dependency injection. 

It's published as part of my blog post: https://relentlesscoding.com/2018/09/02/spring-basics-wiring-and-injecting-beans-with-java-configuration
